const updateTheme = () => {
    const node = document.getElementById("theme");
    node.textContent = "Actuel : " + (() => {
        if (theme == "light") {
            return "Clair";
        } else {
            return "Sombre";
        }
    })();
}

document.getElementById("themeDark").addEventListener("click", () => { setPreferedTheme(theme = "dark"); updateTheme() });
document.getElementById("themeReset").addEventListener("click", () => { setPreferedTheme(theme = getPreferedColorScheme()); updateTheme() });
document.getElementById("themeLight").addEventListener("click", () => { setPreferedTheme(theme = "light"); updateTheme() });

updateTheme();
