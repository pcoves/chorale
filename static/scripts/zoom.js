const updateZoom = () => {
    const node = document.getElementById("zoom");
    node.textContent = "Actuel : " + ((zoom) => {
        if (zoom == 0) { return "Défaut"; }
        else if (zoom == -5) { return "Minimum"; }
        else if (zoom == 5) { return "Maximum"; }
        return zoom;
    })((zoom * 10).toFixed(0));
};

const saveZoom = (zoom) => {
    document.documentElement.style.setProperty("--zoom", Number(zoom));
    localStorage.setItem("zoom", zoom);
    updateZoom();
}

document.getElementById("zoomLess").addEventListener("click", () => {
    zoom = Math.min(Math.max(zoom - .1, -.5), +.5);
    saveZoom(zoom);
});

document.getElementById("zoomReset").addEventListener("click", () => {
    saveZoom(zoom = 0);
});

document.getElementById("zoomMore").addEventListener("click", () => {
    zoom = Math.min(Math.max(zoom + .1, -.5), +.5);
    saveZoom(zoom);
});

updateZoom();
