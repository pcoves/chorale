let zoom = localStorage.getItem("zoom") || "0";
document.documentElement.style.setProperty("--zoom", Number(zoom));

const getPreferedColorScheme = () => {
    if (window.matchMedia("(prefers-color-scheme: dark)").matches) {
        return "dark";
    } else {
        return "light";
    }
};

const setPreferedTheme = (theme) => {
    document.documentElement.setAttribute("data-theme", theme);
    localStorage.setItem("theme", theme);
}

let theme = localStorage.getItem("theme") || getPreferedColorScheme();
setPreferedTheme(theme);
